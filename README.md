# Manga and Webcomic Reader 2000 - READ ME #

### Tutorial available here: https://youtu.be/W4C5QJFmDPQ ###
### See it in action here: http://forums.lolscans.com/book ###

'Manga and Webcomic Reader 2000' is a tool for scanlation groups to publish manga and webcomics on their own site(s). It is not a tool
for clients to download manga to read offline or anything like that.

It aims to be light-weight using efficient and easy-to-understand code yet still retaining original image quality (this can lead to longer
loading times depending on the speed of your server). It is not open for contribution as of right now but can be in the future.

### How do I get set up? ###

* Installation:
You'll need to download this repository and upload it to somewhere in public_html on your web server.

* Configuration:
The things you will need to be concerned with are: 'readerSelector.php', 'projects.cfg' and all the folders.
Quick note before I forget, the only reason you need to be concerned with 'readerSelector.php' is that that's the name of the webpage
that you will use the reader on.

Stick an index.php in there that redirects to readerSelector.php to simplify your URL!

The first thing you'll want to do is open up projects.cfg and add all the names of your projects on the first line (space separated)
(if it goes over the first line then I'm sorry, this won't work, but that's highly unlikely).

On the second line you'll want to put the type of project corresponding with the actual project above it. Don't worry, you can have
as many spaces between the words as you like - this goes for both lines. Note that your project name must not contain spaces.

However! You can put underscores in there and they're automatically converted to spaces in the website!

The project type can be either 'manga' or 'webcomic' without the quotes.

The third line is the name of the folder you're putting the projects you are releasing in. You might be allowed spaces in this but
better safe than sorry.

Now you will want to create your folders for the projects. You will want to create them inside whatever you called your project folder's
name i.e. the folder has the same name as the third line of the projects.cfg file. The names of the folders will be the names of the
projects on the first line of projects.cfg. Again, no spaces in names please.

Now you will want to create your chapter folders for each project, you can name them anything but bear in mind these names will be used
for the chapter selection drop-down box on the web-page.

The images inside the chapters folders can have any name but must be .png or .jpg files (this includes variations on the extensions like
.JPEG). The images will be displayed in alphabetical order.

* Adding a new Project:
Open projects.cfg and add the name of the project to the top row, below it add the type. Add the folder for the project on the server.
Add the chapters folders for the project on the server. Upload the images to the chapter folders.
It's that easy!

### Contribution guidelines etc.###

* I see this as a solo venture but if you wish to assist to create new features please feel free to contact me.
* You are allowed to modify your code for personal use.
* You do not have permission to re-upload this project to the internet, modified or otherwise unless it is for the purposes it was intended.
This means you can use it but you can't steal my code and claim it was your own. Well, I guess you could, I don't have a copyright but that
wouldn't be very nice, now would it?
* You can edit the css file 'styles.css' to your heart's content, it looks particularly good if it looks like the rest of your site, assuming
you have one.

### Who do I talk to? ###

* If you have any questions regarding anything involving this work, do not hesitate to message me at: matthew.j.jenkinson@gmail.com or
PM me on BitBucket if that's a thing.

Note that there is some stuff in here specific to LOLScans, like analytics, donation links, discord and disqus boxes. If you don't want to take them out yourself, ask me to do it. I help any time.


### OTHER (IMPORTANT NEVERTHELESS) ###

Recently I added a way to browse projects by images. These images must be the same names as the project folders and be .png files. They must also be in the same place as the project folders. Not having these thumbnail images doesn't break it but I do recommend using them.

I have added a way to read novels. These are text files (haven't tested different encoding methods but UTF-8 works) with or without extensions. These files go in the same place as the chapter folders as if they are replacing them. In the config, their type is "novella", again without the quotes. One bonus feature is that text links i.e phrases starting http or www are turned into actual links.