<?php
	ob_start();//This if for changing the pagetitle later
?>
<!DOCTYPE html>
<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="styles.css">
		<link rel="icon" type="image/ico" href="favicon.ico">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<meta charset="UTF-8">
		<title>
			<!--TITLE-->
		</title>
	</head>
	
	<!--<body style="background-image:url(tentacles.png)">-->
	<body>
		<?php include_once("analyticstracking.php") ?>
		<script type='text/javascript'>
			function nextImg()
			{
				if(currentIndex < imgArray.length - 1)
				{
					currentIndex++;
					showImage(currentIndex);
				}
			}
			function prevImg()
			{
				if(currentIndex > 0)
				{
					currentIndex--;
					showImage(currentIndex);
				}
			}
			function showImage()
			{
				if(currentIndex == 0)
				{
					document.getElementById("prev").style.visibility = "hidden";
				}
				else
				{
					document.getElementById("prev").style.visibility = "visible";
				}
				if(currentIndex == imgArray.length - 1)
				{
					document.getElementById("next").style.visibility = "hidden";
				}
				else
				{
					document.getElementById("next").style.visibility = "visible";
				}
				if(currentIndex != imgArray.length - 1)
				{
					document.getElementById("pre-load").src = imgArray[currentIndex + 1];//smart browsers will cache this image. - used in manga mode
				//	document.getElementById("disqus_thread").style.visibility = "hidden";
				//	document.getElementById("discord_thread").style.visibility = "hidden";
				}
				else
				{
				//	document.getElementById("disqus_thread").style.visibility = "visible";
				//	document.getElementById("discord_thread").style.visibility = "visible";
				}
				document.getElementById("currentImage").src = imgArray[currentIndex];
				document.getElementById('page_no').innerHTML = "<p>Page: " + currentIndex + " / " + (imgArray.length - 1) 					+ "</p>";
				
				/* Probably shouldn't be doing this everytime we load an image but whatever. I'll move it later. */
				var pagesList;
				for(var i = 0; i < imgArray.length; i++)
				{
					pagesList += "<option value='" + i + "'>" + i + "</option>";	
				}
				document.getElementById("goto_page").innerHTML = pagesList;
				document.getElementById('goto_page').selectedIndex = '-1';
				
				scroll(0,0);
			}
		</script>
		
		<?php
		
			$configFile = fopen("projects.cfg", "r") or die("Could not open the config file.");
			
			$firstLine  = fgets($configFile);
			$secondLine = fgets($configFile);
			$thirdLine  = fgets($configFile);
			
			$startAdsFolder = rtrim(fgets($configFile));
			$endAdsFolder = rtrim(fgets($configFile));
			
			fclose($configFile);
			
			function isMobile() {
				return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
			}
			
			if(isset($_GET["project"]) && isset($_GET["chapter"]) && isset($_GET["pFolder"]) && isset($_GET["type"]))
			{
				$project = $_GET["project"];
				$chapter = $_GET["chapter"];
				$pFolder = $_GET["pFolder"];
				$type    = $_GET["type"];
				
				echo
				"<script type='text/javascript'>
				function openInManga()
				{
					window.open('page2.php?chapter=" . $chapter . "&project=" . $project . "&type=manga&pFolder=" . $pFolder . "', '_self');
				}
				function openInWebcomic()
				{
					window.open('page2.php?chapter=" . $chapter . "&project=" . $project . "&type=webcomic&pFolder=" . $pFolder . "', '_self');
				}
				</script>";
			}
			else
			{
				echo "<h4>[ERROR]: Invalid value for project, chapter, pFolder, or type (was not set). Quitting</h4>";
				echo "<p>This error can only occur when the url is manually changed or if the server is having a REALLY bad day.</p>";
				return;
			}
			
			echo "<script type='text/javascript'>";
			echo "var global_project = \"" . $project . "\";";
			echo "var global_chapter = \"" . $chapter . "\";";
			echo "var global_pFolder = \"" . $pFolder . "\";";
			echo "var global_type = \"" . $type . "\";";
			echo "</script>";
			
			
			//For the previous and next chapter buttons...
			$pChap = "INVALID";
			$nChap = "INVALID";
			
			if(is_dir($pFolder . '/' . $project))
			{
				$chaptersArray = scandir($pFolder . '/' . $project);
			}
			else
			{
				echo "<h4>[ERROR]: This project doesn't exist. Quitting.</h4>";
				echo "<p>This error can only occur when the user modifies the URL directly >:| </p>";
				return;
			}
			
			if(!is_dir($pFolder . '/' . $project . '/' . $chapter) && !file_exists($pFolder . '/' . $project . '/' . 				$chapter))
			{
				echo "<h4>[ERROR]: This particular chapter doesn't exist. Quitting.</h4>";
				echo "<p>This error can only occur when the user modifies the URL directly >:| </p>";
				return;
			}
			
			
			for($i = 0; $i < count($chaptersArray); $i++)
			{
				if($chaptersArray[$i] == $chapter)
				{
					if($i - 1 >= 0)
					$pChap = $chaptersArray[$i - 1];
					if($i + 1 < count($chaptersArray))
					$nChap = $chaptersArray[$i + 1];
				}
			}
			
			$uploadDate = "INVALID_DATE";
			
			if($type=="manga" || $type=="webcomic")
			{
				//Image array of all files (not just images) for this chapter
				$URL     = $pFolder . "/" . $project . "/" . $chapter;
				$imgArry = scandir($URL);
				// Initial value, will be corrected later on...
				$uploadDate = date("l, jS F, Y", filemtime($URL));
				$sAdsArry = scandir($startAdsFolder);
				$eAdsArry = scandir($endAdsFolder);
			}
			elseif($type=="novella")
			{
				$textfileaddress = $pFolder . '/' . $project . '/' . $chapter;
				$textfile = fopen($textfileaddress, 'r') or die('failed to open the text file.');
				$uploadDate = date("l, jS F, Y", filemtime($textfileaddress));
			}
			else
			{
				echo "<h4>[ERROR]: Unrecognised type, expected \"manga\" or \"webcomic\" but found: \"" . $type . "\". 					Quitting.</h4>";
				return;
			}
			
			$pageTitle = $project . " - " . $chapter;	
			$pageTitle = preg_replace('/([\_])+/', ' ', $pageTitle);
			
			//Display single images with buttons to traverse, image can also be clicked.
			if($type=="manga")
			{
				echo "<button onclick='openInWebcomic()'>Switch to webcomic mode</button>";
				
				echo "<script type='text/javascript'>";
				echo "var imgArray = [];";
				for($i = 0; $i < count($imgArry); $i++)
				{
					if($imgArry[$i] == "." || $imgArry[$i] == "..")
					{
						array_splice($imgArry, $i, 2); //Removes them both, hence the 2. I know, I know, I don't know why either.
					}
				}
				for($i = 0; $i < count($imgArry); $i++)
				{
					$imgArry[$i] = $URL . '/' . $imgArry[$i];
				}
				
				$uploadDate = date("l, jS F, Y", filemtime($imgArry[0]));
				
				for($i = 0; $i < count($sAdsArry); $i++)
				{
					if($sAdsArry[$i] != "." && $sAdsArry[$i] != "..")
					{
						array_unshift($imgArry, $startAdsFolder . '/' . $sAdsArry[$i]);
					}
				}
				for($i = 0; $i < count($eAdsArry); $i++)
				{
					if($eAdsArry[$i] != "." && $eAdsArry[$i] != "..")
					{
						array_push($imgArry, $endAdsFolder . '/' . $eAdsArry[$i]);
					}
				}
				$imgArry = array_values($imgArry);
				for($i = 0; $i < count($imgArry); $i++)
				{
					echo "imgArray[" . $i . "] = '" . $imgArry[$i] . "';";
				}
				echo "var currentIndex = 0;";
				
				echo "function goToPage(){";
				echo 	"var imageSelect = document.getElementById('goto_page');";
				echo 	"var selectedImage = imageSelect.options[imageSelect.selectedIndex].text;";
				echo	"currentIndex = selectedImage;";
				echo	"showImage();";
				echo "}";
				
				echo "</script>";
				echo "<table align='center'>";
				echo "<tr><td colspan='3'><img id=\"currentImage\" src=\"" . $imgArry[0] . "\" width='100%' height='auto' onclick=\"nextImg(currentIndex)\"/></td></tr>";
				
				echo 	"<tr>";
				echo 		"<td align='left'>";
				echo 			"<button id=\"prev\" type=\"button\" onclick=\"prevImg(currentIndex)\">&#8592;
				</button>";
				echo		"</td>";
				echo 		"<td align='center' id='page_no'></td>";
				echo 		"<td align='right'>";
				echo 			"<button id=\"next\" type=\"button\" onclick=\"nextImg(currentIndex)\">&#8594;
				</button>";
				echo		"</td>";
				echo 	"</tr>";
				
				echo 	"<tr>";
				echo		"<td align='center' colspan='3'>";
				echo			"<p>Go to page: </p><select id='goto_page' onchange='goToPage()'></select>";
				echo		"</td>";
				echo 	"</tr>";
				echo "</table>";
			}
			elseif($type=="webcomic")//Display images in a list from top to bottom
			{
				echo "<button onclick='openInManga()'>Switch to manga mode</button>";
				
				echo "<table align='center'>";
				for($i = 0; $i < count($sAdsArry); $i++)
				{
					if($sAdsArry[$i] != "." && $sAdsArry[$i] != "..")
					{
						echo "<tr><td>";
						echo "<img src='" . $startAdsFolder . "/" . $sAdsArry[$i] . "'/>";
						echo "</td></tr>";
					}
				}
				for($i = 0; $i < count($imgArry); $i++)
				{
					if($imgArry[$i] != "." && $imgArry[$i] != "..")
					{
						echo "<tr><td>";
						echo "<img src='" . $URL . "/" . $imgArry[$i] . "'/>";	
						echo "</td></tr>";
					}
				}
				$uploadDate = date("l, jS F, Y", filemtime($URL . "/" . $imgArry[3]));
				for($i = 0; $i < count($eAdsArry); $i++)
				{
					if($eAdsArry[$i] != "." && $eAdsArry[$i] != "..")
					{
						echo "<tr><td>";
						echo "<img src='" . $endAdsFolder . "/" . $eAdsArry[$i] . "'/>";
						echo "</td></tr>";
					}
				}
				echo "</table>";
			}
			elseif($type=="novella")//Displays a .txt or extensionless text file.
			{
				echo "<h1 style='text-align: center;'>" . $pageTitle . "</h1><br>";
				if(isMobile())
				{
					//Should I set text-align to justify or to center?
					echo "<p style='text-align: center;
					margin-left: 0%;
					margin-right: 0%;
					padding: 2%;
					background-color: #fff;
					font-size: 32px;'>";
				}
				else
				{
					//they have different margins
					echo "<p style='text-align: center;
					margin-left: 20%;
					margin-right: 20%;
					padding: 2%;
					background-color: #fff;'>";
				}
				
				$disallowed_tags = array();
				$tags_file = fopen('disallowedTags.txt', 'r') or die('failed to open the disallowed tags file.');
				while(!feof($tags_file))
				{
					array_push($disallowed_tags, fgets($tags_file));
				}
				
				//trim just in case
				for($i = 0; $i < count($disallowed_tags); $i++)
				{
					$disallowed_tags[$i] = trim($disallowed_tags[$i]);
				}
				
				while(!feof($textfile))
				{
					
					//put it in a variable so the method "fgets" is not being called in the preg_match operations.
					$line = fgets($textfile);
					
					//Check if the line contains a disallowed tag. Only do so if the line contains a <
					if(preg_match('(<)', $line))
					{
						for($i = 1; $i < count($disallowed_tags); $i++)
						{
							if(preg_match('(.*< *' . $disallowed_tags[$i] . '.*)', $line))
							{
								echo "</p><h4>Disallowed tag used. Please do not use the " . $disallowed_tags[$i] . " tag.</h4>";
								return;
							}
						}
					}
					
					//turn parts of the line with http in them into links - alas it can only find the first link in the line.
					if(preg_match('(http)', $line))
					{						
						$fromStartOfAddress = strstr($line, "http");
						$lineSplitBySpaces  = preg_split('([ ])', $fromStartOfAddress);
						$toLink = trim($lineSplitBySpaces[0]);
						$link = '<a href=' . $toLink . '>' . $toLink . '</a><br>';
						//NB I should only have the BR if a carriage return was trimmed but I am too lazy to check.
						echo str_replace($toLink, $link, $line);
					}
					elseif(preg_match('(www)', $line))
					{
						$fromStartOfAddress = strstr($line, "www");
						$lineSplitBySpaces  = preg_split('([ ])', $fromStartOfAddress);
						$toLink = trim($lineSplitBySpaces[0], "\n\r");
						$link = '<a href=http://' . $toLink . '>' . $toLink . '</a><br>';
						echo str_replace($toLink, $link, $line);
					}
					else
					{
						echo $line;
						
						if(preg_match('([\n])'  , $line) || 
						preg_match('([\r])'  , $line) || 
						preg_match('([\n\r])', $line) || 
						preg_match('([\r\n])', $line)
						)
						{
							echo "<br>";
						}
					}
				}
				echo "</p>";
			}
			echo "<p>";
			echo "This chapter was released on " . $uploadDate . ".";
			echo "</p>";
			echo "<script>
			function next_chapter()
			{";
			if(file_exists($pFolder . '/' . $project . '/' . $nChap) && $nChap != ".." && $nChap != "." && $nChap != "")
			{
				echo "window.open('page2.php?chapter=" . $nChap . "&project=" . $project . "&type=" . $type . 					"&pFolder=" . $pFolder . "', '_self');";
			}	
			echo "}
			function prev_chapter()
			{";
			if(file_exists($pFolder . '/' . $project . '/' . $pChap) && $pChap != ".." && $pChap != "." && $pChap != "")
			{
				echo "window.open('page2.php?chapter=" . $pChap . "&project=" . $project . "&type=" . $type . 					"&pFolder=" . $pFolder . "', '_self');";
			}	
			echo "}
			</script>";
			
			if((file_exists($pFolder . '/' . $project . '/' . $pChap) && $pChap != ".." && $pChap != "." && $pChap != "") || (file_exists($pFolder . '/' . $project . '/' . $nChap) && $nChap != ".." && $nChap != "." && $nChap != ""))
			{
				$otherChapters = scandir($pFolder . '/' . $project);
				
				echo "Other chapters in this series:<br>";
				
				echo "<select id=\"OtherChapters\" onchange=\"goToChapter()\">";
				
				for($i = 0; $i < count($otherChapters); $i++)
				{
					if($otherChapters[$i] != "." && $otherChapters[$i] != ".." && $otherChapters[$i] != $chapter)
					echo "<option value = \"" . $otherChapters[$i] . "\">" . $otherChapters[$i] . "</option>";
				}
				
				echo "</select>";
			}
		?>
		<br><br>
		<script>
			function go_home()
			{
				window.open('readerSelector.php', '_self');
			}
			function togglecomments()
			{
				if(document.getElementById("disqus_thread").style.visibility == "hidden")
				{
					document.getElementById("disqus_thread").style.visibility = "visible";
				}
				else
				{
					document.getElementById("disqus_thread").style.visibility = "hidden";
				}
				
				if(document.getElementById("discord_thread").style.visibility == "hidden")
				{
					document.getElementById("discord_thread").style.visibility = "visible";
				}
				else
				{
					document.getElementById("discord_thread").style.visibility = "hidden";
				}
				
			}
			function goToChapter()
			{
				var chapter = document.getElementById("OtherChapters").value;
				window.open("page2.php?chapter=" + chapter + "&project=" + global_project + "&type=" + global_type + "&pFolder=" + global_pFolder, "_self");
			}
		</script>
		<button type="button" onclick="go_home()">Browse Projects</button><br>
		<?php
			if(file_exists($pFolder . '/' . $project . '/' . $nChap) && $nChap != ".." && $nChap != "." && $nChap != "")
			echo '<button type="button" onclick="next_chapter()">Next Chapter</button><br>';
			else
			echo "<p>This is the last chapter we've released.</p>";
			if(file_exists($pFolder . '/' . $project . '/' . $pChap) && $pChap != ".." && $pChap != "." && $pChap != "")
			echo '<button type="button" onclick="prev_chapter()">Previous Chapter</button><br>';
			else
			echo "<p>This is the first chapter we've released.</p>";
			if($type == "novella")
			{
				echo '<a href="javascript:(function(){function loadScript()%7Bif(console %26%26 typeof(console.log)===%27function%27)%7Bconsole.log(%27SpritzletInit v1.1.8 - Loading https://sdk.spritzinc.com/bookmarklet/latest/js/SpritzletOuter.js%27);}var script=document.createElement(%27script%27);script.setAttribute(%27type%27,%27text/javascript%27);script.setAttribute(%27charset%27,%27UTF-8%27);script.setAttribute(%27async%27,%27true%27);script.setAttribute(%27src%27,%27https://sdk.spritzinc.com/bookmarklet/latest/js/SpritzletOuter.js%3F%27+(new Date().getTime()).toString().substring(0,7));document.documentElement.appendChild(script);setTimeout(function()%7Bif(Spritzlet.timedOut===true)%7Balert(%22Sorry, it looks like this site doesn%27t allow bookmarklets to be run or Spritz servers aren%27t responding.%22);}},3000);script.onload=function()%7BSpritzlet.timedOut=false;var rs=script.readyState;if(!rs || rs===%27loaded%27 || rs===%27complete%27)%7Bscript.onload=script.onreadystatechange=null;Spritzlet.init();}};}if(window.Spritzlet)%7BSpritzlet.activate();}else%7Bwindow.Spritzlet=window.Spritzlet ||%7B};window.Spritzlet=%7Borigin:window.location.protocol+%27//%27+window.location.host,loaderVersion:1.1,timedOut:true};loadScript();}})();">
				Read With Spritz</a>';
			}
			echo "<hr>";
			
			if($type == "manga")
			{
				//echo "<p id='spoiler_message'>To avoid spoilers, comments on manga are hidden until the last page.</p>";
				echo "<button onclick='togglecomments()'>Click here to toggle comments</button>";
			}
			
		?>
		<div id="disqus_thread"></div>
		<script> 
			/* 
				* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM 		OR CMS. * 
			LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables */ 
			
			/* var disqus_config = function () { this.page.url = PAGE_URL;
				// Replace PAGE_URL with your page's canonical URL variable this.page.identifier = PAGE_IDENTIFIER;
			// Replace PAGE_IDENTIFIER with your page's unique identifier variable }; */ 
			//if(!isMobile.any())
			//{
			(function() { // DON'T EDIT BELOW THIS LINE
				var d = document, s = d.createElement('script');
				s.src = '//httpforumslolscanscombookreaderselectorphp.disqus.com/embed.js';
				s.setAttribute('data-timestamp', +new Date()); (d.head || d.body).appendChild(s);
			})(); 
			//}
			document.getElementById("OtherChapters").selectedIndex = "-1";
			// Disable right click
			document.addEventListener('contextmenu', event => event.preventDefault());
			// Disable ctrl + s, ctrl + shift + i and F12
			// Alt + e and the browser's menu cannot be stopped
			onkeydown = function(e)
			{
				if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0) ||
				   e.keyCode == 123 ||
				   e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0) ||
				   e.metaKey && e.keyCode == 'S'.charCodeAt(0) ||
				   e.ctrlKey && e.keyCode == 'U'.charCodeAt(0))
				{
					e.preventDefault();
				}
				
				if(e.keyCode == 'D'.charCodeAt(0) ||
				   e.keyCode == 32 || // space
				   e.keyCode == 39)   // right arrow
				{
					e.preventDefault(); // to prevent scrolling with space
					nextImg(currentIndex);
				}
				
				if(e.keyCode == 'A'.charCodeAt(0) || // A
				   e.keyCode == 37) // left arrow
				{
					prevImg(currentIndex);
				}
			}			
		</script> 
		<noscript>
			Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
		</noscript>
		<script id="dsq-count-scr" src="//httpforumslolscanscombookreaderselectorphp.disqus.com/count.js" async></script>
		
		<div id="discord_thread">
			<iframe src="https://discordapp.com/widget?id=306395174895091712&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0"></iframe>
		</div>
		
		<img id="pre-load" style="visibility: hidden; height: 1px; width: 1px;"/>
		<?php
			if($type == 'manga')
			{
				echo "<script type='text/javascript'>
				// load first time so that the previous button doesn't show on first image when initially loading and the next image will load quicker.
				showImage();
				</script>";
			}
		?>
	</body>
</html>
<!-- Here the page title is changed. All the HTML is loaded into the ob buffer and that string is replaced -->
<?php
	$pageContents = ob_get_contents();
	ob_end_clean();
	echo str_replace("<!--TITLE-->", $pageTitle, $pageContents);
?>						