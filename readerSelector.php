<!DOCTYPE html>
<html>
	
	<head>		
		<!-- NB: Page fails to load when offline because of all these external links. Particularly bootstrap. -->
		<link rel='stylesheet' type='text/css'  href='https://fonts.googleapis.com/css?family=Open+Sans'>
		<link rel="stylesheet" type='text/css'  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type='text/css'  href="styles.css?version=3">
		<link rel="icon" 	   type="image/ico" href="favicon.ico">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		
		<title>
			LOL Reader
		</title>
		<style>
			p.clear
			{
			clear: both;
			}		
		</style>
	</head>
	
	<!--<body style="background-image:url(64x64sample.png)">-->
	<body>		
		<?php include_once("analyticstracking.php") ?>
		<script type="text/javascript">
			
			function getChapters()
			{
				var project = document.getElementById("mySelectP").value;
				var folders = projectFolders[project];
				var optionsString;
				for(var i = 0; i < folders.length; i++)
				{
					optionsString += "<option value='" + i + "'>" + folders[i] + "</option>";	
				}
				document.getElementById("mySelectC").innerHTML = optionsString;
				document.getElementById('mySelectC').selectedIndex = '-1';
				return false;
			}
			
			function getImages()
			{
				var projectSelect   = document.getElementById("mySelectP");
				var selectedProject = projectSelect.options[projectSelect.selectedIndex].text;
				
				var chapterSelect   = document.getElementById("mySelectC");
				var selectedChapter = chapterSelect.options[chapterSelect.selectedIndex].text;
				
				var currentType = tArray[projectSelect.selectedIndex];
				
				selectedProject = selectedProject.replace(/([\ ])+/g, "_");
				window.open("page2.php?chapter=" + selectedChapter + "&project=" + selectedProject + "&type=" + currentType + "&pFolder=" + pFolder, '_self');
			}
			
			function search()
			{
				var input = document.getElementById("search_bar").value;
				var terms = input.toLowerCase().split(" ");
				var found = false;
				var valid = false;
				
				//clear hits
				for(var i = 0; i < all_projects.length; i++)
				{
					all_projects[i].hits = 0;
				}
				
				for(var i = 0; i < terms.length; i++)
				{
					for(var j = 0; j < all_projects.length; j++)
					{
						//Change to if term1 is a match in any AND term2 (etc.) is a match in any. If that's not too difficult.
						var current_project = all_projects[j];
						if(current_project.project.toLowerCase().indexOf(terms[i]) >= 0 ||
						current_project.chapter.toLowerCase().indexOf(terms[i]) >= 0 ||
						current_project.type.toLowerCase().indexOf(terms[i]) >= 0)
						{
							current_project.hits++;
							found = true;
						}
						
					}
				}
				
				for(var i = 0; i < terms.length; i++)
				{
					if(terms[i] == '' || terms[i] == ' ')
					{
						valid = false;
					}
					else
					{
						valid = true;
						break;
					}
				}
				
				var results_string = "Could not find a project with those terms.";
				if(found == true && valid == true)
				{
					all_projects.sort(function(a, b){return b.hits - a.hits});//In theory should sort the array based on number of hits.
					results_string = "<table><tr><th>Top results:</th><th>Hits:</th></tr>";
					for(var i = 0; i < all_projects.length && all_projects[i].hits > 0; i++)
					{
						var p = all_projects[i];
						results_string += "<tr><td><a href='page2.php?chapter=" +
						p.chapter +
						"&project=" +
						p.project.replace(/([\ ])+/g, "_") +
						"&type=" + p.type + "&pFolder=" +
						pFolder + "'>" + p.project + " - " + p.chapter + "</a></td><td>&nbsp;&nbsp;" + p.hits + "</td></tr>";
					}
					results_string += "</table>";
				}
				document.getElementById("search_results").innerHTML = results_string;
			}
			
			function clearSearchBar()
			{
				document.getElementById("search_bar").value = "";
			}
			
			<?php
				$all_projects = array();
			?>
		</script>
		
		<!-- PAGE STARTS HERE -->
		
		<h1>
			LOLScans' Reader
		</h1>
		
		<button type="button" onclick="window.open('https://forums.lolscans.com/','_self')">LOLScans Forums</button>
		
		<button type="button" onclick="surprise_me()">Surprise Me!</button>
		
		<p>
			<ul>
				<li>If you find any problems with a particular chapter please contact us via email, forum post, or leave a message on that chapter.</li>
			</ul>
		</p>
		
		<input type="text" id="search_bar" onkeypress="search()" onfocus="clearSearchBar()">
		
		<p id="search_results">
		</p>
		
		<?php
			$configFile = fopen("projects.cfg", "r") or die("Could not open the config file.");
			
			$firstLine  = fgets($configFile);
			$secondLine = fgets($configFile);
			$thirdLine  = fgets($configFile);
			
			fclose($configFile);
			
			
			$firstLine  = preg_replace('/([\ ])+/', ' ', $firstLine);//replace multiple spaces with a single space.
			$secondLine = preg_replace('/([\ ])+/', ' ', $secondLine);
			
			$firstLine  = rtrim($firstLine); //removes the newLine character
			$secondLine = rtrim($secondLine);//and any pesky spaces the user might've left in.
			$thirdLine = rtrim($thirdLine);
			
			$pArray = explode(" ", $firstLine);	//create an array of project names
			$tArray = explode(" ", $secondLine);//create an array of types
			
			echo "<script type='text/javascript'>";
			echo "var tArray = [";
			for($i = 0; $i < count($tArray); $i++)
			{
				if($i > 0)
				{
					echo ",";
				}
				echo "\"" . $tArray[$i] . "\"";
			}
			echo "];var pFolder =\"" . $thirdLine . "\";</script>";
			
			
			if(count($pArray) != count($tArray))
			{
				echo "<h4>[ERROR]: The number of categories does not match the number of projects. Quitting.</h4>";
				return;
			}
			
			$pFolder = $thirdLine; //superfluous but makes more sense in current context.
			
			if(is_dir($pFolder))
			{
				//check to see if all the folders listed in the projects.cfg file exist.
				for($i = 0; $i < count($pArray); $i++)
				{
					if(!(is_dir($pFolder . '/' . $pArray[$i])))
					{
						echo "<h3>[WARNING]: The folder for \"" . $pArray[$i] . "\" does not exist.<br></h3>";
						//remove the project from the list if it doesn't exist.
						unset($pArray[$i]);
						//this is probably the thing that causes it to break when there's an extra unused folder in there.
					}
				}
				
				//start table for neat formatting
				echo "<table>";
				echo 	"<tr>";
				echo		"<td>Project</td><td>Chapter</td>";
				echo	"</tr>";
				echo 	"<tr>";
				echo		"<td>";
				
				//create a form so we can post some data.
				echo "<form action=\"\" method=\"POST\">";
				//create first drop-down (projects). Also turns underscores into spaces :)
				echo "<select id=\"mySelectP\" onchange=\"getChapters()\">";//call the JS method to getChapters.
				for($i = 0; $i < count($pArray); $i++)
				{
					echo "<option value=\"" . $i . "\">" . preg_replace('/([\_])/', ' ', $pArray[$i]) . "</option>";
				}
				echo "</select>";
				
				echo		"</td>";
				echo		"<td>";
				
				//chapters drop-down goes here
				echo "<form action=\"\" method=\"POST\">";
				echo "<select id=\"mySelectC\" onchange=\"getImages()\"></select>";
				echo "</form>";
				
				echo 		"</td>";
				echo	"</tr>";
				echo "</table>";
				
				//generate JS
				echo "<script type='text/javascript'>";	
				echo "var projectFolders = [];";
				for($i = 0; $i < count($pArray); $i++)
				{
					echo "var folderContents" . $i . " = [];";
					$folders = scandir($pFolder . '/' . $pArray[$i]);
					for($j = 2; $j < count($folders); $j++)//2 is used because the first 2 folders (elements [0] and [1]) are . and .. //however, folder names should not be alphabetically before .
					{
						$k = $j - 2;
						echo "folderContents" . $i . "[" . $k . "] = '" . $folders[$j] . "';";
					}
					echo "projectFolders[" . $i . "] = folderContents" . $i . ";";
				}
				echo "</script>";
				
				$useragent=$_SERVER['HTTP_USER_AGENT'];
				function isMobile()
				{
					return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
				}
				
				//Add all the projects to an array, [chapter, project, type];
				echo "<script type='text/javascript'>";
				echo "var all_projects = [];";
				for($i = 0; $i < count($pArray); $i++)
				{
					$type = $tArray[$i];
					
					$folders = scandir($pFolder . '/' . $pArray[$i]);
					
					if(file_exists($pFolder . "/" . $pArray[$i] . ".png"))
					{
						for($j = 0; $j < count($folders) - 2; $j++)
						{
							$k = $j + 2;
							//PHP array, used in getting a random project (and probably other things).
							$project_entry = array($folders[$k], preg_replace('/([\ ])/', '_', $pArray[$i]), $type);
							array_push($all_projects, $project_entry);
							//JS Array, used for the search. The 0 is for a hit counter for a relevance value.
							echo "all_projects.push({chapter:'" . $folders[$k] . "', project:'" . preg_replace('/([\_])/', ' ', $pArray[$i]) . "', type:'" . $type . "', hits:0});";
						}
					}
				}
				echo "</script>";
				
				//Display the images by categories and by projects
				$types_array = array("manga", "webcomic", "novella");
				$names_array = array("Mangas:", "Webcomics:", "Light Novels:");//Names. Attention! If you are using this in another language feel free to change this!
				for($k = 0; $k < count($types_array); $k++)
				{
					$data_content_string = "";
					echo "<hr><b>" . $names_array[$k] . "</b><br><br>";
					for($i = 0; $i < count($all_projects); $i++)
					{
						$j = $i + 1;
						$current_project = $all_projects[$i];
						if($i !== count($all_projects) - 1)
						$next_project    = $all_projects[$j];
						if($current_project[2] == $types_array[$k])
						{
							$LINK = "&quot;page2.php?chapter=" . $current_project[0] . '&amp;project=' . $current_project[1] . '&amp;type=' . $current_project[2] . '&amp;pFolder=' . $pFolder . "&quot;";
							$data_content_string = $data_content_string . "<a href=" . $LINK . ">" . $current_project[0] . '</a><br>';
							if($i == count($all_projects) - 1 || $next_project[1] !== $current_project[1])
							{
								//if statement lets the last one through without looking at $next_project (which doesn't exist... because it's the last one.)
								//used mainly not to clog up my error log
								echo "<img tabindex='" . $i . "'
								src='" . $pFolder . "/" . $current_project[1] . ".png'
								class='thumb'
								href='#'
								title='" . preg_replace('/([\_])/', ' ', $current_project[1]) . "'
								data-toggle='popover'
								data-html='true'";
								if(isMobile())
								echo "data-placement='auto down'";
								else
								echo "data-placement='auto right'";
								echo "
								data-content='" . $data_content_string . "'
								/>";
								$data_content_string = "";
							}
						}
					}
				}
			}
			else
			{
				echo "<h4>[ERROR]: Could not find projects folder. Quitting.<br></h4>";
				echo "Expected to find: " . $pFolder;
				return;
			}
		?>
		<br/><br/>
		<left>
			<script type='text/javascript' src='https://ko-fi.com/widgets/widget_2.js'></script><script type ='text/javascript'>kofiwidget2.init('Buy Me a Coffee', '#009cde', 'A320DUO');kofiwidget2.draw();</script> 
			Keep us alive by buying us a cup of coffee~ C': 
		</left>
		<br><br>
		<p class="clear"><small>
			If you would like to use this style of reader for your own site please visit <a href='https://bitbucket.org/40163650/manga-and-webcomic-reader-2000'>The Bitbucket page for it</a>.
		</small></p>
		<script type='text/javascript'>
			window.addEventListener("resize", resizeImages);
			var isMobile = {
				Android: function() {
					return navigator.userAgent.match(/Android/i);
				},
				BlackBerry: function() {
					return navigator.userAgent.match(/BlackBerry/i);
				},
				iOS: function() {
					return navigator.userAgent.match(/iPhone|iPad|iPod/i);
				},
				Opera: function() {
					return navigator.userAgent.match(/Opera Mini/i);
				},
				Windows: function() {
					return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
				},
				any: function() {
					return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
				}
			};
			
			document.getElementById("mySelectP").selectedIndex = "-1";
			
			function resizeImages()
			{
				var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
				document.body.style.minWidth = w + "px";
				if(isMobile.any())
				{
					var imagesArray = document.getElementsByClassName("thumb");
					for(var i = 0; i < imagesArray.length; i++)
					{
						imagesArray[i].style.width = (w - 80) + "px";
						imagesArray[i].style.height = "auto";
					}
				}
			}
			
			document.getElementById("search_bar").value = "Type to search...";
			
			resizeImages();
			
			$(document).ready(function(){
				$('[data-toggle="popover"]').popover()
			});
			
			// Disable right click
			document.addEventListener('contextmenu', event => event.preventDefault());
			// Disable ctrl + s, ctrl + shift + i and F12
			// Alt + e and the browser's menu cannot be stopped
			onkeydown = function(e)
			{
				if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0) ||
				   e.keyCode == 123 ||
				   e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0) ||
				   e.metaKey && e.keyCode == 'S'.charCodeAt(0) ||
				   e.ctrlKey && e.keyCode == 'U'.charCodeAt(0))
				{
					e.preventDefault();
				}
			}
		</script>
		<?php
			$min = 0;
			$max = count($all_projects) - 1; //because element 0 is the 1st...
			$rand_index = rand($min, $max); 
			$rand_chapter = $all_projects[$rand_index];
			$r_chapter = $rand_chapter[0];
			$r_project = $rand_chapter[1];
			$r_type    = $rand_chapter[2];
			echo "<!--Predetermined random chapter is: " . $r_project . ", " . $r_chapter . ", Element: " . $rand_index . "-->";
			echo "<script>";
			echo 	"function surprise_me(){";
			echo	"window.open(\"page2.php?chapter=" . $r_chapter .
			"&project=" . $r_project .
			"&type="    . $r_type    .
			"&pFolder=" . $pFolder   .
			"\", \"_self\");";
			echo 	"}";
			echo "</script>";
		?>
	</body>
</html>																
