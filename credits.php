<!DOCTYPE html>
<html>
	<head>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" href="styles.css">
		<link rel="icon" type="image/ico" href="favicon.ico">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<meta charset="UTF-8">
		<title>
			Credits Pages
		</title>
		
		<style type="text/css">
			figure{
				margin: 0;
				text-align: center;
				padding-left: 4px;
				padding-right: 4px;
				padding-up: 2px;
				padding-down: 2px;
				display: inline-block;
			}
			img{
				width: 80px;
			}
			figcaption{
				font-size: 8px;
			}
			td{
				padding: 4px;
			}
		</style>
		<script type="text/javascript">
			function openInNewTab(imgAddress)
			{
				window.open(imgAddress, '_blank');
			}
		</script>
	</head>
	
	<body>
		<p>
			Please note that this a page for staff use, to help us keep a record of who did what. It's public because it has no reason not to be.
		</p>
		<p>
			Click on an image or its caption to view it at full size.
		</p>
		<?php
			$configFile = fopen("projects.cfg", "r") or die("Could not open the config file.");
			
			$firstLine  = fgets($configFile);
			$secondLine = fgets($configFile);
			$thirdLine  = fgets($configFile);
			
			$startAdsFolder = rtrim(fgets($configFile));
			$endAdsFolder = rtrim(fgets($configFile));
			
			fclose($configFile);
			
			$firstLine  = preg_replace('/([\ ])+/', ' ', $firstLine);//replace multiple spaces with a single space.
			$secondLine = preg_replace('/([\ ])+/', ' ', $secondLine);
			
			$firstLine  = rtrim($firstLine); //removes the newLine character
			$secondLine = rtrim($secondLine);//and any pesky spaces the user might've left in.
			$thirdLine = rtrim($thirdLine);
			
			$pArray = explode(" ", $firstLine);	//create an array of project names
			$tArray = explode(" ", $secondLine);//create an array of types
			
			$mangaAndWebcomicArray = array();
			
			$j = 0;
			for($i = 0; $i < count($pArray); $i++)
			{
				if($tArray[$i] == "manga" || $tArray[$i] == "webcomic")
				{
					$mangaAndWebcomicArray[$j] = $pArray[$i];
				}
				$j++;
			}
			
			$pFolder = $thirdLine;
			
			$creditsImagesArray = array();
			// To be: projectFolderName/Alice_Royale/CH 01/AR_ch001_p000c.jpg etc.
			
			$chaptersArray = array();
			$imagesArray = array();
			
			echo "<p>";
			
			if(is_dir($pFolder))
			{
				//check to see if all the folders listed in the projects.cfg file exist.
				for($i = 0; $i < count($mangaAndWebcomicArray); $i++)
				{
					if(is_dir($pFolder . '/' . $mangaAndWebcomicArray[$i]))
					{
						$currentChaptersArray = scandir($pFolder . '/' . $mangaAndWebcomicArray[$i]);
						
						for($j = 0; $j < count($currentChaptersArray); $j++)
						{
							if($mangaAndWebcomicArray[$i] != "" && $mangaAndWebcomicArray[$i] != "." && $mangaAndWebcomicArray[$i] != ".." &&
							   $currentChaptersArray[$j]  != "" && $currentChaptersArray[$j]  != "." && $currentChaptersArray[$j]  != "..")
							{
								$currentChaptersArray[$j] = $pFolder . '/' . $mangaAndWebcomicArray[$i] . '/' . $currentChaptersArray[$j];
								$currentImagesArray = scandir($currentChaptersArray[$j]);

								for($k = 0; $k < count($currentImagesArray); $k++)
								{
									$currentImagesArray[$k] = $currentChaptersArray[$j] . '/' . $currentImagesArray[$k];
									// echo "i: " . $i . ". j: " . $j . ". k:" . $k . ". currentImagesArray[k]: " . $currentImagesArray[$k] . "<br>";
									if($k == 2)
									{
										array_push($creditsImagesArray, $currentImagesArray[$k]);
									}
								}
							}
						}
						$imagesArray = array_unique(array_merge($imagesArray, $currentImagesArray));
						$chaptersArray = array_unique(array_merge($chaptersArray, $currentChaptersArray)); // Like += but done properly.
					}
				}
			}
			
			echo "</p>";
			
			$thisProject = "";
			$lastProject = "";
			$projectCounter = 0;
			for($i = 0; $i < count($creditsImagesArray); $i++)
			{
				$stringBits = explode("/", $creditsImagesArray[$i]);
				$chapterName = $stringBits[2];
				$thisProject = $stringBits[1];
				if($thisProject != $lastProject)
				{
					echo "<hr>";
					$projectCounter++;
				}
				echo "<figure onclick='openInNewTab(\"" . $creditsImagesArray[$i] . "\")'>";
				echo "<img src='" . $creditsImagesArray[$i] . "'/>";
				echo "<figcaption>" . $chapterName . "</figcaption>";
				echo "</figure>";
				$lastProject = $thisProject;
			}
			echo "<hr><table><tr><td>";
			echo $projectCounter . "</td><td>Manga and Webcomic Projects</td></tr><tr><td>" . count($creditsImagesArray) . "</td><td>Total chapters";
			echo "</td></tr></table>";
		?>		
	</body>
</html>